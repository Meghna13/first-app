import React from 'react';
import { getUser, removeUserSession } from './Utils/Common';
import MovieList from './MovieList';
import  { MovieProvider } from './MovieContext';
 
function Dashboard(props) {
  const user = getUser();
 
  const handleLogout = () => {
    removeUserSession();
    props.history.push('/login');
  }
 
  return (
    <div>
      Welcome {user.name}!<br /><br />
      <MovieProvider>
        <div>
          <MovieList/>
        </div>
      </MovieProvider>
      <input type="button" onClick={handleLogout} value="Logout" />

    </div>

  );
}
 
export default Dashboard;